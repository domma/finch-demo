import json
import functools
import logging
import inspect

import falcon
from jinja2 import Environment, FileSystemLoader, select_autoescape
from requests_toolbelt.multipart import decoder

LOG = logging.getLogger(__file__)

env = Environment(
    loader=FileSystemLoader("./templates"),
    autoescape=select_autoescape(["html", "xml"]),
)


class Page:
    def __init__(self, template, **args):
        self.template = template
        self.args = args

    def render(self, resp):
        resp.content_type = falcon.MEDIA_HTML
        resp.body = env.get_template(self.template).render(**self.args)


class Action:
    def __init__(self, target, template, action="replace"):
        self.action = action
        self.target = target
        self.template = template
        self._data = {}

    def args(self, **args):
        self._data = args
        return self

    def render(self):
        inner = env.get_template(self.template).render(**self._data)

        return f"""
        <turbo-stream action="{self.action}" target="{self.target}">
            <template>
            {inner}
            </template>
        </turbo_stream>"""


class FinchRequest:
    def __init__(self, data={}):
        self.data = data

    @property
    def action(self):
        return self.data.get("action")

    @property
    def args(self):
        return self.data.get("args")


class Turbo:
    def process_resource(self, req, resp, resource, params):
        if req.content_type and req.content_type.startswith("multipart/form-data"):
            parts = decoder.MultipartDecoder(req.stream.read(), req.content_type).parts
            for p in parts:
                disp = p.headers.get(b"Content-Disposition")
                if disp:
                    disp = disp.decode("ascii")
                    #   Look up proper way to parse those headers. This
                    #   solution is probably very "hackish", but works
                    #   fine for the proof of concept.
                    if not disp.startswith("form-data;"):
                        continue
                    name = disp.split("name=")[1][1:-1]
                    value = p.content
                    if name == "finch.action":
                        params["finchAction"] = FinchRequest(data=json.loads(value))
                    else:
                        params[name] = value.decode("utf-8")


def turbo(fkt):
    def handler(self, req, resp, *args, **kwargs):
        result = fkt(self, req, resp, *args, **kwargs)

        if type(result) == Page:
            result.render(resp)

        elif inspect.isgenerator(result):
            resp.content_type = "text/html; turbo-stream"
            resp.body = "\n".join([i.render() for i in result])

    return handler
