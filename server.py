import os.path
import json
from collections import Counter
import random

import falcon
import finch


class Search:
    def __init__(self):
        with open("cities.json") as f:
            self.cities = json.load(f)

    @finch.turbo
    def on_get(self, req, resp):
        return finch.Page(
            "index.jinja2", activeFilters=[], number=random.randint(0, 100)
        )

    @finch.turbo
    def on_post(self, req, resp, prefix, finchAction, activeFilters):
        if activeFilters:
            activeFilters = json.loads(activeFilters)
        else:
            activeFilters = []

        if finchAction.action == "addFilter":
            activeFilters.append(finchAction.args)

        result = [
            c
            for c in self.cities
            if c["name"].lower().startswith(prefix)
            and ((not activeFilters) or c["land"] in activeFilters)
        ]

        groups = Counter(c["land"] for c in result)
        result = result[:5]

        yield finch.Action("filters", "filters.jinja2").args(
            activeFilters=activeFilters
        )

        yield finch.Action("results", "results.jinja2").args(
            items=result, groups=groups
        )
        yield finch.Action("random", "random.jinja2").args(
            number=random.randint(0, 100)
        )


api = falcon.API(middleware=[finch.Turbo()])
search = Search()
api.add_route("/", search)
api.add_static_route("/static", os.path.abspath("static"))
