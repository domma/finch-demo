import * as Turbo from "https://cdn.skypack.dev/pin/@hotwired/turbo@v7.0.0-beta.1-tPCW1AHTtVGOe5r89LWv/min/@hotwired/turbo.js" 

var application = Stimulus.Application.start()

application.register("finch", class extends Stimulus.Controller {
    connect() {
        let ctrl = this;

        var box = ctrl.element.getElementsByClassName("search-text")[0];

        for (let i of this.element.getElementsByTagName("input")) {
            console.log(i);
            if (i.getAttribute("type")=="hidden" && i.getAttribute("name")=="finch.action") {
                this.finchStatus = i;
            }
        }

        if (!this.finchStatus) {
            console.warn("Could not find hidden status field for finch actions.");
        }

        box.addEventListener("input", function(e) {
            if (ctrl.delayed_update) {
                clearTimeout(this.delayed_update);
                ctrl.delayed_update = undefined;
            }
            ctrl.delayed_update = setTimeout(function() {
                ctrl.finchAction("search");
                ctrl.delayed_update = undefined;
            }, 500);
        });
    }

    add_filter(e) {
        let filter = e.currentTarget.getAttribute("data-filter");
        this.finchAction("addFilter", filter);
    }

    finchAction(action, args) {
        let data = {};
        if (this.finchStatus.value) {
            data = JSON.parse(this.finchStatus.value);
        }
        data.action = action;
        data.args = args;
        this.finchStatus.value = JSON.stringify(data);
        this.element.requestSubmit();
    }
});
